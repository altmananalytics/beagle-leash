# beagle-leash

## License

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

## Introduction

`beagle-leash` is a harness for automating some of the drudgery involved in using the `beagle` software for imputation of ungenotyped markers/SNPs.

Some of the work automated includes:

* Automatic removal of wildtype markers, to avoid Beagle bug (reported to Prof. Brown)
* Separates an input VCF file into per-chromosome files for "imputable" chromosomes (i.e. 1-22 & X)
* Imputes each chromosome file
* Merges the imputed results
* Inserts wildtype markers (overwriting any mistakenly-imputed
  versions) from input file
* Y & MT markers are inserted into the output VCF file
* All input loci not included in the Beagle output file are re-inserted
  (Beagle doesn't output loci not found in reference data)

The end result is that `beagle-leash` provides you with a single VCF file
that includes all of your original loci, augmented by imputed loci.

## Prerequisites

* GNU make
* GNU awk (`gawk`)
* Working Java environment for Beagle

## Installation

Run `make install` to install both software and reference data. 

This will also add a line to your `.bashrc` script to add the install
path to your `$PATH` environment variable. You can open a new shell
for the `beagle-leash` path to take effect, or you can evalute the
line added to the `.bashrc` script in the current shell to have the
path extended, and ready for immediate use.

Run `make install-nodata` to install just the software. This is useful
to do if you are caching the large reference files locally. The
directory path of the BREF-formatted reference files will then need to
be supplied to the program by setting the value of the
`BEAGLE_REFDB_PATH` environment variable. For example:

```
export BEAGLE_REFDB_PATH=$PWD/inst/beagle-leash/ref-data
```

The default installation target is a directory called `inst` at the
top-level of the git repository. To override this setting, specify an
alternate installation directory on the command line, as follows:

```
make DESTDIR=/my/path install
```


## Running

We can run `beagle-leash` as follows:

```
beagle-leash input-file.vcf.gz imputed-output.vcf.gz
```

`beagle-leash` defaults to using a single core, but it can run more
efficiently by processing each chromosome in parallel if more cores are provided. This can be done simply by
providing a third argument that specifies the number of targets that
`make` should process in parallel:

```
beagle-leash input-file.vcf.gz imputed-output.vcf.gz 16
```

`beagle-leash` makes use of a temporary directory for performing the
various bits of work. The temporary directory selection takes into
consideration whether the standard Unix `TMPDIR` environment variable
is set. By default it will create a temporary directory under
`/tmp`. Again, this can be passed to the pipeline by setting & exporting
the `TMPDIR` on the command line in the current shell as below:

```
export TMPDIR=/dev/shm
```

## TODO

* Beagle currently strips out some header lines. Need to add ability to put them back in.
* `beagle-leash` currently has hard-coded chromosome list, unless manually overridden with an environmental variable. This should instead be auto-generated from the unique chromosome IDs in the input file.
* Sometimes `beagle` will strip out RSIDs from the output. These should be repaired using the input file as reference.
* `beagle` requires that all 'male non-pseudoautosomal X-chromosome genotypes to be coded as homozygous diploid genotypes', as per the Beagle docs. We should repair these entries in the output so that they are again non-pseudoautosomal.
