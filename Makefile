#### beagle-leash installation Makefile

## This Source Code Form is subject to the terms of the Mozilla Public
## License, v. 2.0. If a copy of the MPL was not distributed with this
## file, You can obtain one at http://mozilla.org/MPL/2.0/.



### Make configuration:
SHELL := /bin/bash
.SHELLFLAGS = -ec

.ONESHELL:

### Local definitions:
version           = 27Jan18.7e1
software-url      = http://faculty.washington.edu/browning/beagle/beagle.$(version).jar
ref-data-dir-url  = http://bochet.gcc.biostat.washington.edu/beagle/1000_Genomes_phase3_v5a/b37.bref/



## The default installation target. Override on the command line / environment variable for alternate location:
DESTDIR  ?= $(CURDIR)/inst/beagle-leash

## Targets:

install: $(DESTDIR)/bin/beagle-leash $(DESTDIR)/ref-data/.complete

install-nodata: $(DESTDIR)/bin/beagle-leash
	@echo "'beagle-leash' installed without downloading reference datasets"
	@echo "Reference dataset files will be assumed to be in $(DESTDIR)/ref-data "
	@echo "unless an alternate location is provided using the 'BEAGLE_REFDB_PATH'"
	@echo "environment variable prior to running beagle-leash."


$(DESTDIR)/bin/beagle-leash: $(DESTDIR)/bin/beagle.$(version).jar
	cp bin/run-beagle-pipeline.make $(DESTDIR)/bin/
	sed 's:===beagle-install-path===:$(DESTDIR):g' bin/beagle-leash \
		| sed 's/===beagle-version===/$(version)/g' \
		> $(DESTDIR)/bin/beagle-leash
	chmod 755 $(DESTDIR)/bin/beagle-leash
	grep $(DESTDIR)/bin $(HOME)/.bashrc \
		|| echo "## beagle-leash: Insert beagle-leash path:"
	grep $(DESTDIR)/bin $(HOME)/.bashrc \
		|| echo "export PATH=$$PATH:$(DESTDIR)/bin" >> $(HOME)/.bashrc
	@echo "Installation complete"
	@echo "Either start a new shell or update your path as below to start using beagle-leash:"
	@echo "export PATH=$$PATH:$(DESTDIR)/bin"



$(DESTDIR)/bin/beagle.$(version).jar:
	mkdir -p $(DESTDIR)/bin
	cd $(DESTDIR)/bin \
		&& wget $(software-url)

$(DESTDIR)/ref-data/.complete:
	mkdir -p $(DESTDIR)/ref-data
	cd $(DESTDIR)/ref-data \
		&& wget --recursive \
			--no-parent \
			--reject "index.html*" \
			--no-host-directories \
			--no-directories \
			--continue $(ref-data-dir-url) \
		&& touch $@


.PHONY: test install install-nodata


test:
	mkdir -p test/output
	export PATH=$$PATH:$(DESTDIR)/bin
	export BEAGLE_LEASH_CHROMS="21 22"
	beagle-leash test/ref/test-chrs-21-22.vcf.gz test/output/chrs-21-22-imputed.vcf.gz
	@echo "How many loci for test VCF input:"
	gunzip < test/ref/test-chrs-21-22.vcf.gz | awk -F"\t" '!/^#/ { print $$1}' | sort | uniq -c | sort -k 2,2
	@echo "How many loci for imputed VCF output:"
	gunzip < test/output/chrs-21-22-imputed.vcf.gz  | awk -F"\t" '!/^#/ { print $$1}' | sort | uniq -c | sort -k 2,2
	@echo "Are there any entries in the input that are not in the output?"
	awk -F"\t" \
		'NR==FNR { beagle_output_markers[$$1,$$2,$$4]; next } \
		 !/^#/ && !(($$1,$$2,$$4) in beagle_output_markers)' \
		<(zcat test/output/chrs-21-22-imputed.vcf.gz ) \
		<(zcat test/ref/test-chrs-21-22.vcf.gz ) \
		| wc -l
# @echo "Are there any position-ID pairs in the output that occur more than once?"
# zcat test/output/chrs-21-22-imputed.vcf.gz | awk -F"\t" '{a[$2,$3]++}END{ for (key in a) if (a[key]>1) print key }' | wc -l

fast-test:
	mkdir -p test/output
	export PATH=$$PATH:$(DESTDIR)/bin
	export BEAGLE_LEASH_CHROMS="21"
	zcat test/ref/test-chrs-21-22.vcf.gz \
		| awk -F"\t" '/^#/ || $$1 == 21' \
		| gzip > test/ref/test-chrs-21.vcf.gz
	date; time beagle-leash test/ref/test-chrs-21.vcf.gz test/output/chrs-21-imputed.vcf.gz; echo "Return code is: $$?"; date 
	@echo "How many loci for test VCF input:"
	zcat test/ref/test-chrs-21.vcf.gz | awk -F"\t" '!/^#/ { print $$1}' | sort | uniq | wc -l
	@echo "How many loci for imputed VCF output:"
	zcat test/output/chrs-21-imputed.vcf.gz  | awk -F"\t" '!/^#/ { print $$1}' | sort | uniq | wc -l
	@echo "Are there any entries in the input that are not in the output?"
	awk -F"\t" \
		'NR==FNR && $$1 != 22 { beagle_output_markers[$$1,$$2,$$4,$$5]; next } \
		 !/^#/ && !(($$1,$$2,$$4,$$5) in beagle_output_markers)' \
		<(zcat test/output/chrs-21-imputed.vcf.gz ) \
		<(zcat test/ref/test-chrs-21.vcf.gz ) \
		| wc -l
	@echo "Are there any position-ID pairs in the output that occur more than once?"
	zcat test/output/chrs-21-imputed.vcf.gz | awk -F"\t" '{a[$$2,$$3]++}END{ for (key in a) if (a[key]>1) print key }' | wc -l




test/ref/example-23andme.txt:
	mkdir -p test/ref
	cd test/ref
	wget -O example-23andme.zip https://my.pgp-hms.org/user_file/download/3511
	unzip example-23andme.zip
	rm example-23andme.zip
	tr -d '\r' *_v5_Full_*.txt > example-23andme.txt
	awk -F"\t" '/^#/ || $$2 == X' example-23andme.txt > example-chrX-23andme.txt

test-chrX: test/ref/test-chrX.vcf.gz
	mkdir -p test/output
	export PATH=$$PATH:$(DESTDIR)/bin
	export BEAGLE_LEASH_CHROMS="X"
	date; time beagle-leash test/ref/test-chrX.vcf.gz test/output/chrX-imputed.vcf.gz; echo "Return code is: $$?"; date 
	@echo "How many loci for test VCF input:"
	zcat test/ref/test-chrX.vcf.gz | awk -F"\t" '!/^#/ { print $$1, $$2}' | sort | uniq | wc -l
	@echo "How many loci for imputed VCF output:"
	zcat test/output/chrX-imputed.vcf.gz  | awk -F"\t" '!/^#/ { print $$1, $$2}' | sort | uniq | wc -l
	@echo "Are there any entries in the input that are not in the output?"
	awk -F"\t" \
		'NR==FNR { beagle_output_markers[$$1,$$2,$$4,$$5]; next } \
		 !/^#/ && !(($$1,$$2,$$4,$$5) in beagle_output_markers)' \
		<(zcat test/output/chrX-imputed.vcf.gz ) \
		<(zcat test/ref/test-chrX.vcf.gz ) \
		| wc -l
	@echo "Are there any position-ID pairs in the output that occur more than once?"
	zcat test/output/chrX-imputed.vcf.gz | awk -F"\t" '{a[$$1,$$2,$$3,$$4,$$5]++}END{ for (key in a) if (a[key]>1) print key }' | wc -l

clean:
	rm -rf $(DESTDIR)/bin/beagle-leash
	rm -f hs_err_pid*.log
