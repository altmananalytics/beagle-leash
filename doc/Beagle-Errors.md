# Errors

## License

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Homozygous reference loci incorrectly imputed as different genotypes

We have three files: `sample.vcf.gz`, which is the genotype dataset
from a genotyping array, in gzip'ed VCF format; `chr21.vcf.gz`, which
is a version of `sample.vcf.gz` with just the entries from chromosome
21, modulo the homozygous reference entries, which are removed; and
`chr21-gt-gprobs.vcf.gz`, which is the output of Beagle imputing
`chr21.vcf.gz` using the provided BREF reference files.

How many rows are there in the input file?
```
zcat ../sample.vcf.gz | awk -F"\t" '$1 == 21' | egrep -c -v "^#"
```
Answer: 

How many rows of the input file have a genotype of homozygous reference?

```
zcat ../sample.vcf.gz | awk -F"\t" '$1 == 21 && $5 == "." { count++}END{print count}'
```
Answer: 4,381

How many input homozygous reference loci are in output as imputed?
```
time awk -F"\t" 'NR==FNR { wildtype_loci[$1,$2]; next } ($1,$2) in wildtype_loci' \
	<(zcat ../sample.vcf.gz | awk -F"\t" '$1 == 21 && $5 == "."') \
	<(zcat chr21-gt-gprobs.vcf.gz) \
| wc -l
```
Answer: 3,896


What is the distribution of genotypes for input homozygous reference
loci in the output?

```
time awk -F"\t" 'NR==FNR { wildtype_loci[$1,$2]; next} \
	             ($1,$2) in wildtype_loci { print $10 }' \
	<(zcat ../sample.vcf.gz | awk -F"\t" '$1 == 21 && $5 == "."') \
	<(zcat chr21-gt-gprobs.vcf.gz) \
| cut -d: -f 1 | sort | uniq -c | sort -n
```
Answer:
```
     11 1|1
     81 1|0
    125 0|1
   3678 0|0
```

So there is no large-scale issue with predicting the incorrect genotype.


## Input VCF lines mysteriously lost in output

We have three files: `sample.vcf.gz`, which is the genotype dataset
from a genotyping array, in gzip'ed VCF format; `chr21.vcf.gz`, which
is a version of `sample.vcf.gz` with just the entries from chromosome
21, modulo the homozygous reference entries, which are removed; and
`chr21-gt-gprobs.vcf.gz`, which is the output of Beagle imputing
`chr21.vcf.gz` using the provided BREF reference files.

How many rows are there in the input file?
```
zcat ../sample.vcf.gz | awk -F"\t" '$1 == "21"' | egrep -c -v "^#"
```
8455

We check if there are any chromosome 21 IDs in the sample.vcf.gz
file that are not found in the Beagle output:

```
time awk -F"\t" 'NR==FNR { out_ids[$3]; next} !($3 in out_ids)' \
	<(zcat chr21-gt-gprobs.vcf.gz ) \
	<(zcat ../sample.vcf.gz | awk -F"\t" '$1 == "21"') \
| wc -l
```

Indeed, we find 546 such examples.

We check to see how many of those entries remain when we remove
entries with genotype "homozygous reference", which we know will not
be in the Beagle output due to a bug in either the software or the
documentation:

```
time awk -F"\t" 'NR==FNR { out_ids[$3]; next} !($3 in out_ids)' \
	<(zcat chr21-gt-gprobs.vcf.gz ) \
	<(zcat ../sample.vcf.gz | awk -F"\t" '$1 == "21"') \
| egrep -v "0/0" | wc -l
```

47 of the 546 rows remain. Some of those rows have an INFO column
value of NOT_DETERMINED, which is not PASS, and thus possibly Beagle
is discarding these values. In this version I remove those values as well:

```
time awk -F"\t" 'NR==FNR { out_ids[$3]; next} !($3 in out_ids)' \
	<(zcat chr21-gt-gprobs.vcf.gz ) \
	<(zcat ../sample.vcf.gz | awk -F"\t" '$1 == "21"') \
| egrep -v "0/0" |  egrep -v "NOT_DETERMINED" | wc -l
```

This removes ten such entries, leaving us with 37 entries discarded
for no known reason. Some of these entries use internal IDs instead of
dbSNP "rs#" IDs (though this shouldn't be a problem, as there are
plenty such entries in `chr21-gt-gprobs.vcf.gz`). I remove them to see
how much this whittles down the list:

```
time awk -F"\t" 'NR==FNR { out_ids[$3]; next} !($3 in out_ids)' \
	<(zcat chr21-gt-gprobs.vcf.gz ) \
	<(zcat ../sample.vcf.gz | awk -F"\t" '$1 == "21"') \
| egrep -v "0/0" |  egrep -v "NOT_DETERMINED" \
| awk -F"\t" '!($3 ~ /^i/)' | wc -l 
```

This whittles the list down to 27. I cannot figure out why these 27
entries are not found in the Beagle output.
