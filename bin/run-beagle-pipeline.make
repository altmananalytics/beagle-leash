#### run-beagle-pipeline.make

##  This Source Code Form is subject to the terms of the Mozilla Public
##  License, v. 2.0. If a copy of the MPL was not distributed with this
##  file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Make Configs:

## Use Bash shell, and exit on error on any recipe line:
SHELL := /bin/bash
.SHELLFLAGS = -ec 

TMPDIR ?= /tmp


## This makes all recipe lines execute within a shared shell process:
## https://www.gnu.org/software/make/manual/html_node/One-Shell.html#One-Shell
.ONESHELL:

## If a recipe contains an error, delete the target:
## https://www.gnu.org/software/make/manual/html_node/Special-Targets.html#Special-Targets
.DELETE_ON_ERROR:

## This is necessary to make sure that these intermediate files aren't clobbered:
.SECONDARY: $(chromosomes-vcf-path)


### Local Definitions

## DO NOT change this to any other assignment, like ?= or =, as this will lead to hard-to-trace bugs.
WORK_DIR := $(shell mktemp -d -p $(TMPDIR) beagle-leash-XXX )
beagle-bin-path = $(BEAGLE_INSTALL_PATH)/bin/beagle.$(BEAGLE_VERSION).jar

## The symbols for the chromosomes that we will impute:
## This can be overridden via setting the environment variable for testing purposes, since it is faster to just test running beagle-leash on chromosomes 21 and 22 rather than all of them.
BEAGLE_LEASH_CHROMS ?= $(shell seq 1 22; echo X)

## The list of per-chromosome VCF files:
chromosomes-vcf              = $(addsuffix .vcf.gz, $(addprefix chr, $(BEAGLE_LEASH_CHROMS)))
chromosomes-vcf-path         = $(addprefix $(WORK_DIR)/sample-chrs/, $(chromosomes-vcf))

## The list of per-chromosome VCF files post-imputation:
imputed-chromosomes-vcf      = $(addsuffix -gt-gprobs.vcf.gz, $(addprefix chr, $(BEAGLE_LEASH_CHROMS)))
imputed-chromosomes-vcf-path = $(addprefix $(WORK_DIR)/imputed-chrs/, $(imputed-chromosomes-vcf))

# ## The list of per-chromosome imputed VCF files with wildtype calls spliced back in:
# imputed-spliced-chromosomes-vcf      = $(addsuffix -gt-gprobs-spliced.vcf.gz, $(addprefix chr, $(BEAGLE_LEASH_CHROMS)))
# imputed-spliced-chromosomes-vcf-path = $(addprefix $(WORK_DIR)/spliced-chrs/, $(imputed-spliced-chromosomes-vcf))

# ## The list of per-chromosome VCF files with only the wildtype markers:
# wildtype-marker-chromosomes-vcf      = $(addsuffix -wildtype-only.vcf-noheader, $(addprefix chr, $(BEAGLE_LEASH_CHROMS)))
# wildtype-marker-chromosomes-vcf-path = $(addprefix $(WORK_DIR)/spliced-chrs/, $(wildtype-marker-chromosomes-vcf))


### Targets

## Does the following:
## 1. Copy the header for the VCF file:
## 2. Copy the wildtype markers from the sample-chrs VCF file
## 3. Copy over input VCF file lines not found in imputed output
## 4. Copy the imputed VCF file except for wildtype markers already copied over
## 5. Sort VCF data lines by chromosome position
## 6. Concatenate the header & body of the VCF, then compress it
## ^^^ I think the above logic is wrong, and more complex than it needs to be; the correct thing to do is:
##     1. Copy over the whole input VCF content
##     2. Copy over the imputed VCF content where it doesn't overlap with the input file

# $(WORK_DIR)/spliced-chrs/chr%-gt-gprobs-spliced.vcf.gz: $(WORK_DIR)/imputed-chrs/chr%-gt-gprobs.vcf.gz $(WORK_DIR)/sample-chrs/chr%.vcf.gz
# 	cd $(WORK_DIR)/spliced-chrs
# 	zcat `echo "$^" | cut -d' ' -f 1` | egrep "^#"    > $@.tmp-header
# 	zcat `echo "$^" | cut -d' ' -f 2` | egrep -v "^#" > $@.tmp-body
# 	awk -F"\t" \
# 		'NR==FNR { input_loci[$$1,$$2,$$4,$$5]; next } \
# 		 !/^#/ && !(($$1,$$2,$$4,$$5) in input_loci)' \
# 		<(zcat `echo "$^" | cut -d' ' -f 2`) \
# 		<(zcat `echo "$^" | cut -d' ' -f 1`) \
# 		>> $@.tmp-body
# 	sort --key 2,2n $@.tmp-body > $@.tmp-body-sorted
# 	cat $@.tmp-header $@.tmp-body-sorted | gzip > $@
# awk -F"\t" \
# 	'NR==FNR { beagle_output_markers[$$1,$$2,$$4,$$5]; next } \
# 	 !/^#/ && !(($$1,$$2,$$4,$$5) in beagle_output_markers)' \
# 	<(zcat `echo "$^" | cut -d' ' -f 2`) \
# 	<(zcat `echo "$^" | cut -d' ' -f 3`) \
# 	>> $@.tmp-body
#rm $@.tmp*


$(WORK_DIR)/imputed-chrs/chr%-gt-gprobs.vcf.gz: $(WORK_DIR)/sample-chrs/chr%.vcf.gz $(BEAGLE_REFDB_PATH)/chr%.1kg.phase3.v5a.bref
	java -jar \
		$(beagle-bin-path) \
	 	gt=`echo "$^"  | cut -d' ' -f 1` \
	 	ref=`echo "$^" | cut -d' ' -f 2` \
	 	gprobs=true \
	 	out=`echo $@ | sed 's/.vcf.gz//g'`

## 1. Copy the first pre-req file to the tmp file
## 2. For all other pre-req files, copy over without the header section
## 3. Add back in the Y, MT, and non-recombinant X loci
## 4. Sort the file by chromosome and position and remove duplicate lines
## 5. 'gzip' the output
## 6. Remove the temp file
$(WORK_DIR)/imputed-sample.vcf.gz: $(imputed-chromosomes-vcf-path)
	zcat `echo "$^" | cut -d' ' -f 1` > $@.tmp
	for file in `echo "$^" | cut -d' ' -f 2-`; do
		egrep -v "^#" <(zcat $$file) >> $@.tmp
	done
	awk -F"\t" \
	 	'NR==FNR { beagle_output_markers[$$1,$$2,$$4,$$5]; next } \
		 !/^#/ && !(($$1,$$2,$$4,$$5) in beagle_output_markers)' \
	 	$@.tmp \
		<(zcat $(WORK_DIR)/sample.vcf.gz) \
	 	>> $@.tmp
	awk -F"\t" '/^#/ { print > "$@-header-only"; next } 1' $@.tmp \
		| sort -k1,1 -k 2,2n \
		| uniq > $@-body.vcf
	cat $@-header-only $@-body.vcf | gzip > $@
	rm $@.tmp $@-header-only $@-body.vcf
#	zcat $(WORK_DIR)/sample.vcf.gz \
#		| awk -F"\t" '!/^#/ && length($$10) == 1' >> $@.tmp
#	zcat $(WORK_DIR)/sample.vcf.gz \#
#		| awk -F"\t" '$$1 == "Y" || $$1 == "MT"' >> $@.tmp


## Part of the logic below includes removing "wildtype" SNPs, as these cause Beagle to break.
## Wish it were more graceful, but it makes sense; wildtype SNPs don't help with imputation, AFAICT.
## Actually, confirmed with Dr. Brown at UW that this is a bug.
## Now we need logic to splice these removed entries back in post-imputation...
## Support for this approach comes form this Biostars question, though there's no evidence provided to back up the commentor's assertion:
## https://www.biostars.org/p/184415/

## We save the rows of the sample's wildtype markers, for splicing in later:
## Things that this step cleans up in the input file instead of just copying over:
## 1. Removes non-recombinatory X chromosome loci
## 2. Removes wildtype loci

$(chromosomes-vcf-path): $(WORK_DIR)/sample.vcf.gz
	awk -F"\t" \
		-v filename=`basename $@` \
		'BEGIN { OFS="\t"; chrom = gensub(/chr([12]?[0-9X]).vcf.gz/, "\\1", "g", filename)} \
		 $$1 == "X" && $$5 != "." && length($$10) == 1 { $$10 = ($$10 "/" $$10); print} \
		 /^#/ || ($$1 == chrom && $$5 != ".") { print }' \
		<(zcat $^) \
	| gzip > $@



# $(wildtype-marker-chromosomes-vcf-path): $(WORK_DIR)/sample.vcf.gz
# 	awk -F"\t" \
# 		-v filename=`basename $@` \
# 		'BEGIN { OFS="\t"; chrom = gensub(/chr([12]?[0-9X])-wildtype-only.vcf-noheader/, "\\1", "g", filename)} \
# 		 $$1 == chrom && $$5 == "." { $$8 = "."; print }' \
# 		<(zcat $^) \
# 		> $@


$(WORK_DIR)/sample.vcf.gz:
	mkdir -p $(WORK_DIR)/sample-chrs $(WORK_DIR)/imputed-chrs
	cp $(BEAGLE_INPUT_FILE) $(WORK_DIR)/sample.vcf.gz


process: $(WORK_DIR)/imputed-sample.vcf.gz
	cp $^ $(BEAGLE_OUTPUT_FILE)
	@echo "beagle-leash completed"
	@echo "Please look for the imputed genotype file in gzip'ed VCF format in the following path:"
	@echo $(BEAGLE_OUTPUT_FILE)

#$(WORK_DIR)/spliced-chrs/chr22-gt-gprobs-spliced.vcf.gz
#$(imputed-chromosomes-vcf-path)
#$(WORK_DIR)/spliced-chrs/chr22-wildtype-only.vcf-noheader


## Utility Targets:
print-env:
	@echo SHELL is $(SHELL)
	@echo TMPDIR is $(TMPDIR)
	@echo WORK_DIR is $(WORK_DIR)
	@echo beagle-bin-path is $(beagle-bin-path)
	@echo BEAGLE_LEASH_CHROMS is $(BEAGLE_LEASH_CHROMS)
	@echo chromosomes-vcf is $(chromosomes-vcf)
	@echo chromosomes-vcf-path is $(chromosomes-vcf-path)
	@echo imputed-chromosomes-vcf is $(imputed-chromosomes-vcf)
	@echo imputed-chromosomes-vcf-path is $(imputed-chromosomes-vcf-path)
#	@echo imputed-spliced-chromosomes-vcf is $(imputed-spliced-chromosomes-vcf)
#	@echo imputed-spliced-chromosomes-vcf-path is $(imputed-spliced-chromosomes-vcf-path)
#	@echo wildtype-marker-chromosomes-vcf is $(wildtype-marker-chromosomes-vcf)
#	@echo wildtype-marker-chromosomes-vcf-path is $(wildtype-marker-chromosomes-vcf-path)


clean-temp-dirs:
	rm -rf $(TMPDIR)/beagle-leash*

